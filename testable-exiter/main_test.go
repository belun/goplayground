package testable_exiter

import (
	"testing"
)

func Test(t *testing.T) {
	var mockedCrasher MockedCrasher = "testable-exiter-crasher"
	var collaborator Collaborator = Collaborator {
		"testable-exiter-collaborator",
		mockedCrasher }
	Main{ "testable-exiter-main", collaborator }.Run()
}
