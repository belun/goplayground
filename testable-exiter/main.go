package testable_exiter

import (
	"fmt"
)

type Main struct {
	Name         string
	Collaborator Collaborator
}

func (main Main) Run() {
	defer func() {
		fmt.Printf("\n[%v] wrapping up.", main.Name)
	}()

	fmt.Printf("\n[%v] running...", main.Name)
	main.Collaborator.doSomething()
	fmt.Printf("\n[%v] done.", main.Name)
}

func RunSample() {
	var crasher Crasher = "testable-exiter-crasher"
	var collaborator Collaborator = Collaborator {
		"testable-exiter-collaborator",
		crasher }
	Main{ "testable-exiter-main", collaborator }.Run()
}



