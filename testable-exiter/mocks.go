package testable_exiter

import "fmt"

type MockedCrasher string

func (crasher MockedCrasher) Exit() {
	fmt.Printf("\n[%v] was supposed to exit !", crasher)
}
