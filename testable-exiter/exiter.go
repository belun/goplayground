package testable_exiter

import (
	"os"
	"fmt"
)

type Exiter interface {
	Exit()
}

type Crasher string

func (crasher Crasher) Exit() {
	fmt.Printf("\n[%v] going to exit... good bye", crasher)
	os.Exit(1)
	fmt.Printf("\n[%v] done.", crasher)
}
