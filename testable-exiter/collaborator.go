package testable_exiter

import (
	"fmt"
	"time"
)

type doer interface {
	doSomething()
}

type Collaborator struct {
	Name string
	Exiter Exiter
}

func (collaborator *Collaborator) doSomething() {
	fmt.Printf("\n[%v] doing something...", collaborator.Name)
	time.Sleep(2 * time.Second)

	collaborator.Exiter.Exit()
	fmt.Printf("\n[%v] done.", collaborator.Name)
}
