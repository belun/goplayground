package main

import (
	"playground/exiter"
	texiter "playground/testable-exiter"
)

func main() {
	exiter.RunSample()
	texiter.RunSample()
}
