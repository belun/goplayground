package exiter

import (
	"fmt"
)

type Main struct {
	Name         string
	Collaborator Collaborator
}

func (main Main) Run() {
	defer func() {
		fmt.Printf("\n[%v] wrapping up.", main.Name)
	}()

	fmt.Printf("\n[%v] running...", main.Name)
	main.Collaborator.doSomething()
	fmt.Printf("\n[%v] done.", main.Name)
}

func RunSample() {
	var collaborator Collaborator = "exiter-collaborator"
	Main{ "exiter-main", collaborator }.Run()
}