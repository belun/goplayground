package exiter

import (
	"fmt"
	"time"
	"os"
)

type doer interface {
	doSomething()
}

type Collaborator string

func (collaborator *Collaborator) doSomething() {
	fmt.Printf("\n[%v] doing something...", *collaborator)
	time.Sleep(2 * time.Second)

	fmt.Printf("\n[%v] going to exit... good bye", *collaborator)
	os.Exit(1)
	fmt.Printf("\n[%v] done.", *collaborator)
}
